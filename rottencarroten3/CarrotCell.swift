//
//  CarrotCell.swift
//  rottencarroten3
//
//  Created by James on 30/07/2020.
//  Copyright © 2020 James. All rights reserved.
//

import UIKit

class CarrotCell: UITableViewCell {

    @IBOutlet weak var checkMarkImage: UIImageView!
    @IBOutlet weak var carrotLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
