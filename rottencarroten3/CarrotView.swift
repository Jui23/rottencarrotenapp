//
//  CarrotView.swift
//  rottencarroten3
//
//  Created by James on 29/07/2020.
//  Copyright © 2020 James. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

struct Carrot {
    var isChecked: Bool
    var carrotName: String
}

class CarrotView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var carrotTV: UITableView!
    
    var carrots: [Carrot] = []
    
    var userID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        carrotTV.delegate = self
        carrotTV.dataSource = self
        carrotTV.rowHeight = 80
        
        setWelcomeLabel()
        
        if let uid = userID {
            welcomeLabel.text = uid
        }
        
        loadCarrots()
        
        setupAvatar()
        // Do any additional setup after loading the view.
    }
    func setWelcomeLabel() {
        let userRef =
            Database.database().reference(withPath: "users").child(userID!)
        userRef.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let email = value!["email"] as? String
            self.welcomeLabel.text = "Hello " + email! + "!"
        }
    }
    
    func setupAvatar() {
        avatar.layer.cornerRadius = 40
        avatar.clipsToBounds = true
        avatar.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(presentPicker))
        avatar.addGestureRecognizer(tapGesture)
    }
    
    @objc func presentPicker() {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    
    @IBAction func logOutAction(_ sender: Any) {
        try! Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func addCarrot(_ sender: Any) {
        
        let carrotAlert = UIAlertController(title: "New Product", message: "Add a product", preferredStyle: .alert)
        
        carrotAlert.addTextField()
        let addCarrotAction = UIAlertAction(title: "Add", style: .default) { (action) in
            let carrotText = carrotAlert.textFields![0].text
            self.carrots.append(Carrot(isChecked: false, carrotName: carrotText!))
            let ref = Database.database().reference(withPath: "users").child(self.userID!).child("carrots")
            ref.child(carrotText!).setValue(["isChecked": false])
            self.carrotTV.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        
        carrotAlert.addAction(addCarrotAction)
        carrotAlert.addAction(cancelAction)
        
        present(carrotAlert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadCarrots() {
        let ref = Database.database().reference(withPath: "users").child(userID!).child("carrots")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children.allObjects as! [DataSnapshot] {
                let carrotName = child.key
                
                let carrotRef = ref.child(carrotName)
                
                carrotRef.observeSingleEvent(of: .value) { (carrotSnapshot) in
                    let value = carrotSnapshot.value as? NSDictionary
                    let isChecked = value!["isChecked"] as? Bool
                    self.carrots.append(Carrot(isChecked: isChecked!, carrotName: carrotName))
                    self.carrotTV.reloadData()
                }
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carrots.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carrotCell", for: indexPath) as! CarrotCell
        
        cell.carrotLabel.text = carrots[indexPath.row].carrotName
        
        if carrots[indexPath.row].isChecked {
            cell.checkMarkImage.image = UIImage(named: "checkmark.png")
        }
        else {
            cell.checkMarkImage.image = nil
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let ref = Database.database().reference(withPath: "users").child(userID!).child("carrots").child(carrots[indexPath.row].carrotName)
        
        if carrots[indexPath.row].isChecked {
            carrots[indexPath.row].isChecked = false
            ref.updateChildValues(["isChecked": false])
        }
        else {
            carrots[indexPath.row].isChecked = true
            ref.updateChildValues(["isChecked": true])
        }
        
        carrotTV.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let ref = Database.database().reference(withPath: "users").child(userID!).child("carrots").child(carrots[indexPath.row].carrotName)
            
            ref.removeValue()
            carrots.remove(at: indexPath.row)
            carrotTV.reloadData()
        }
    }
}

