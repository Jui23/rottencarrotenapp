//
// ViewController.swift
//  rottencarroten3
//
//  Created by James on 29/07/2020.
//  Copyright © 2020 James. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import UserNotifications

class ViewController: UIViewController {
	
	@IBOutlet weak var emailTF: UITextField!
	
	@IBOutlet weak var passwordTF: UITextField!
	
	var uid: String = ""
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		emailTF.text = ""
		passwordTF.text = ""
		//Do any additional setup after loading the view, typically from a nib.
        
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            
            
        let content = UNMutableNotificationContent()
            content.title = "RottenCarroten"
            content.body = "Remember about your food product!"
            
            let date = Date().addingTimeInterval(10)
            
            let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
            
            let uuidString = UUID().uuidString
            
            let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
            
            center.add(request) { (error) in
                
            }
        }
	}
	
    override func viewWillAppear(_ animated: Bool) {
		emailTF.text = ""
		passwordTF.text = ""
	}
	
	
	
	@IBAction func registerAction(_ sender: Any) {
	
		if emailTF.text != nil && passwordTF.text != nil {
			Auth.auth().createUser(withEmail: emailTF.text!, password: passwordTF.text!) { (result, error) in
				if error != nil {
					print("THERE WAS AN ERROR")
				}
				else {
					self.uid = (result?.user.uid)!
					let ref = Database.database().reference(withPath: "users").child(self.uid)
					ref.setValue(["email" : self.emailTF.text!, "password": self.passwordTF.text!])
					self.performSegue(withIdentifier: "loginSegue", sender: self)
				}
			}
		}
	}
		
		
		
	@IBAction func signInAction(_ sender: Any) {
		if emailTF.text != nil && passwordTF.text != nil {
			Auth.auth().signIn(withEmail: emailTF.text!, password: passwordTF.text!) { (result, error) in
				if error != nil {
					print("THERE WAS AN ERROR")
				}
				else {
					self.uid = (result?.user.uid)!
					self.performSegue(withIdentifier: "loginSegue", sender: self)
				}
			}
		}
	}
		
		
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let navigation = segue.destination as! UINavigationController
		let carrotVC = navigation.topViewController as! CarrotView
		carrotVC.userID = uid
	}
}
					
